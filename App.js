import React, { Component } from 'react';
import { StyleSheet, Text, View, FlatList, Image, Picker, Platform } from 'react-native';

import players from './players.json';

const GENDERS = [
  { label: 'Female', value: 'Female' },
  { label: 'Male', value: 'Male' }
]

const SKILLS = [
  { label: 'Beginner', value: 'Beginner' },
  { label: 'Intermediate', value: 'Intermediate' },
  { label: 'Advanced', value: 'Advanced' }
]

export default class App extends Component {

  state = {
    gender: null,
    skill: null
  }

  _renderPickerItems = items => items.map(item => <Picker.Item label={item.label} value={item.value} key={`item-${item.value}`} />)

  _renderItem = ({ item }) => (
    <View style={styles.playerWrapper}>
      <Image source={{ uri: item.avatar }} style={styles.avatar} />
      <View style={styles.detailsWrapper}>
        <Text style={styles.text}>{item.name}</Text>
        <View style={styles.infoWrapper}>
          <Text style={styles.infoText}>{item.gender}</Text>
          <Text style={styles.infoText}>{item.skill}</Text>
        </View>
      </View>
    </View>
  )

  _renderSeparator = () => (<View style={styles.separator} />)

  _applyFilters = () => {
    let filteredPlayers = [...players];
    if (this.state.gender) {
      filteredPlayers = filteredPlayers.filter(player => player.gender === this.state.gender);
    }
    if (this.state.skill) {
      filteredPlayers = filteredPlayers.filter(player => player.skill === this.state.skill);
    }
    return filteredPlayers;
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.controlsWrapper}>
          <Picker
            selectedValue={this.state.gender}
            style={{ flex: 1 }}
            onValueChange={gender => this.setState({ gender })}>
            {this._renderPickerItems(GENDERS)}
          </Picker>
          <Picker
            selectedValue={this.state.skill}
            style={{ flex: 1 }}
            onValueChange={skill => this.setState({ skill })}>
            {this._renderPickerItems(SKILLS)}
          </Picker>
        </View>
        <FlatList
          style={{ marginTop: Platform.select({ android: 0, ios: 150 }) }}
          data={this._applyFilters()}
          keyExtractor={item => item.id.toString()}
          renderItem={this._renderItem}
          ItemSeparatorComponent={this._renderSeparator}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  playerWrapper: {
    flexDirection: "row",
    alignItems: 'center',
    width: '100%',
    height: 80,
    backgroundColor: '#efefef',
    padding: 10
  },
  avatar: {
    width: 60,
    height: '100%',
    backgroundColor: '#cdcdcd',
    borderRadius: 30,
    marginRight: 10
  },
  separator: {
    width: '100%',
    height: 5
  },
  text: {
    fontSize: 18,
    letterSpacing: .8,
    fontWeight: '700',
    flex: 1
  },
  infoWrapper: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  detailsWrapper: {
    flex: 1,
  },
  infoText: {
    flex: 1
  },
  controlsWrapper: {
    width: '100%',
    height: 50,
    flexDirection: 'row'
  }
});
